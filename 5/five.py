from computer import Computer

with open("5.in", "r") as file:
    prog = [int(x) for x in file.readlines()[0].split(",")]

Computer(prog.copy()).execute()
