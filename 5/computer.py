from enum import IntEnum
from instruction import Instruction
from opcodes import OPCodes


class InvalidAccessModeException(BaseException):
    pass


class Mode(IntEnum):
    POSITION = 0
    IMMEDIATE = 1


class Computer:
    def __init__(self, mem):
        self.__mem = mem
        self.__ip = 0

    def __increment_ip(self, inc):
        self.__ip += inc

    def __write_to_location(self, loc, val):
        self.__mem[loc] = val

    def __val_at_location(self, loc):
        return self.__mem[loc]

    def __current_instruction(self):
        return self.__val_at_location(self.__ip)

    def __val_at_offset(self, offset):
        return self.__val_at_location(self.__ip + offset)

    def __val_at_offset_by_mode(self, mode, offset):
        loc = self.__ip + offset
        if mode == Mode.IMMEDIATE:
            return self.__val_at_location(loc)
        elif mode == Mode.POSITION:
            return self.__val_at_location(self.__val_at_location(loc))
        else:
            raise InvalidAccessModeException

    def __execute_instruction(self):
        cont = True
        nxt_dist = 4
        inst = Instruction(self.__current_instruction())
        if inst.operator == OPCodes.ADD:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__val_at_offset(3)
            self.__write_to_location(dest, lhs+rhs)
        elif inst.operator == OPCodes.MULT:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__val_at_offset(3)
            self.__write_to_location(dest, lhs*rhs)
        elif inst.operator == OPCodes.LT:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__val_at_offset(3)
            self.__write_to_location(dest, 1 if lhs < rhs else 0)
        elif inst.operator == OPCodes.EQ:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__val_at_offset(3)
            self.__write_to_location(dest, 1 if lhs == rhs else 0)
        elif inst.operator == OPCodes.JMPT:
            chk = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            nxt_dist = 3 if chk == 0 else (self.__val_at_offset_by_mode(inst.rhs_mode, 2) - self.__ip)
        elif inst.operator == OPCodes.JMPF:
            chk = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            nxt_dist = 3 if chk != 0 else (self.__val_at_offset_by_mode(inst.rhs_mode, 2) - self.__ip)
        elif inst.operator == OPCodes.IN:
            read = int(input("Input int: "))
            dest = self.__val_at_offset(1)
            self.__write_to_location(dest, read)
            nxt_dist = 2
        elif inst.operator == OPCodes.OUT:
            src = self.__val_at_offset(1)
            print(self.__val_at_location(src))
            nxt_dist = 2
        elif inst.operator == OPCodes.HALT:
            cont = False
        self.__increment_ip(nxt_dist)
        return cont

    def execute(self,):
        while self.__execute_instruction():
            pass
