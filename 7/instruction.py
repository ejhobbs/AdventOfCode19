from opcodes import OPCodes


class Instruction:
    def __init__(self, opcode):
        op_string = str(opcode)
        if len(op_string) < 5:
            while len(op_string) < 5:  # pad front with 0s
                op_string = "0" + op_string

        operator = int(op_string[3:5])
        if not OPCodes.is_valid(operator):
            raise InvalidOpcodeException
        self.operator = operator
        self.lhs_mode = int(op_string[2])
        self.rhs_mode = int(op_string[1])


class InvalidOpcodeException(BaseException):
    pass
