from computer import Computer
from status_code import StatusCode
from itertools import permutations

with open("7.in", "r") as file:
    prog = [int(x) for x in file.readlines()[0].split(",")]

initial_input = 0
available_phases = 5
phase_sequences = permutations(range(5, 10))

outputs = []
for seq in phase_sequences:
    A = Computer("A", prog.copy(), seq[0])
    B = Computer("B", prog.copy(), seq[1])
    C = Computer("C", prog.copy(), seq[2])
    D = Computer("D", prog.copy(), seq[3])
    E = Computer("E", prog.copy(), seq[4])
    [x.execute() for x in [A, B, C, D, E]]  # initialise every machine at once
    A.input(0)
    A.execute()
    finished = False
    while not finished:
        B.input(A.get_output())
        B.execute()
        C.input(B.get_output())
        C.execute()
        D.input(C.get_output())
        D.execute()
        E.input(D.get_output())
        E.execute()
        fin = E.get_output()
        if E.execute() == StatusCode.HALT:
            outputs.append(fin)
            finished = True
        else:
            A.input(fin)
            A.execute()

print(max(outputs))
