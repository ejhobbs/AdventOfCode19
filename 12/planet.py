class Planet:
    def __init__(self, line):
        pairs = [x.strip() for x in line.split(",")]
        x = int(pairs[0].split("=")[1])
        y = int(pairs[1].split("=")[1])
        z = int(pairs[2].split("=")[1])
        self.pos = [x, y, z]
        self.vel = [0, 0, 0]

    def __str__(self):
        return f"{self.pos[0]}{self.pos[1]}{self.pos[2]}{self.vel[0]}{self.vel[1]}{self.vel[2]}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.pos == other.pos and self.vel == other.vel

    def delete_pos(self, p):
        self.pos[p] = 0
        self.vel[p] = 0

    def __energy(self, prop):
        x, y, z = prop
        return abs(x) + abs(y) + abs(z)

    def energy(self):
        return self.__energy(self.vel) * self.__energy(self.pos)

    def update_velocity(self, deltas):
        x, y, z = self.vel
        dx, dy, dz = deltas
        self.vel = (x + dx, y + dy, z + dz)

    def step(self):
        x, y, z = self.pos
        dx, dy, dz = self.vel
        self.pos = (x + dx, y + dy, z + dz)

    def apply_gravity(self, body):
        self_delta = [0, 0, 0]
        body_delta = [0, 0, 0]
        for i in range(3):
            if self.pos[i] > body.pos[i]:
                body_delta[i] += 1
                self_delta[i] -= 1
            elif self.pos[i] < body.pos[i]:
                body_delta[i] -= 1
                self_delta[i] += 1
        self.update_velocity(self_delta)
        body.update_velocity(body_delta)
