from planet import Planet
from copy import deepcopy


def process_input(lines):
    return [Planet(x.strip()[1:-1]) for x in lines]


def hash_system(moons):
    hash = ""
    for moon in moons:
        hash += f"{moon.pos},{moon.vel},"
    return hash


def time_step(moons, pairings):
    for pair in pairings:
        a = moons[pair[0]]
        b = moons[pair[1]]
        a.apply_gravity(b)
    for moon in moons:
        moon.step()


with open("12.in", "r") as file:
    moons = process_input(file.readlines())

pairings = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)]
moon_triple = [[deepcopy(moons[0]), deepcopy(moons[1]), deepcopy(moons[2]), deepcopy(moons[3])] for i in range(3)]
## delete y & z for first set

moon_triple[0][0].delete_pos(1)
moon_triple[0][0].delete_pos(2)
moon_triple[0][1].delete_pos(1)
moon_triple[0][1].delete_pos(2)
moon_triple[0][2].delete_pos(1)
moon_triple[0][2].delete_pos(2)
moon_triple[0][3].delete_pos(1)
moon_triple[0][3].delete_pos(2)

## delete x & z for first set
moon_triple[1][0].delete_pos(0)
moon_triple[1][0].delete_pos(2)
moon_triple[1][1].delete_pos(0)
moon_triple[1][1].delete_pos(2)
moon_triple[1][2].delete_pos(0)
moon_triple[1][2].delete_pos(2)
moon_triple[1][3].delete_pos(0)
moon_triple[1][3].delete_pos(2)

## delete x & y for first set
moon_triple[2][0].delete_pos(0)
moon_triple[2][0].delete_pos(1)
moon_triple[2][1].delete_pos(0)
moon_triple[2][1].delete_pos(1)
moon_triple[2][2].delete_pos(0)
moon_triple[2][2].delete_pos(1)
moon_triple[2][3].delete_pos(0)
moon_triple[2][3].delete_pos(1)

steps = []
for moon_set in moon_triple:
    init = deepcopy(moon_set)
    time = 0
    while True:
        time_step(moon_set, pairings)
        time += 1
        if moon_set == init:
            break
    steps.append(time)

print(steps)
