from planet import Planet


with open("12.in", "r") as file:
    moons = [Planet(x.strip()[1:-1]) for x in file.readlines()]

pairings = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)]

for i in range(1000):
    for pair in pairings:
        a = moons[pair[0]]
        b = moons[pair[1]]
        a.apply_gravity(b)
    for moon in moons:
        moon.step()

print(f"Total energy after 1000 steps is: {sum([moon.energy() for moon in moons])}")
