import math

with open("1.in", "r") as file:
    print(sum([math.floor(int(x) / 3) - 2 for x in file.readlines()]))
