from enum import IntEnum
from math import inf
from instruction import Instruction
from opcodes import OPCodes
from status_code import StatusCode


class InvalidAccessModeException(BaseException):
    pass


class Mode(IntEnum):
    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2



class Computer:
    def __init__(self, name, mem):
        self.name = name
        self.__mem = mem
        self.__mem_end = len(mem)
        self.__overflow = {}
        self.__ip = 0

        self.__rel = 0

        self.__input_ready = False
        self.__input_value = inf

        self.__output_consumed = False
        self.__output_value = inf

        self.__status = StatusCode.CONTINUE

    def __increment_ip(self, inc):
        self.__ip += inc

    def __increment_rel(self, val):
        self.__rel += val

    def __write_to_location(self, loc, val):
        if loc >= self.__mem_end:
            dest = self.__overflow
        else:
            dest = self.__mem
        dest[loc] = val

    def __val_at_location(self, loc):
        if loc >= self.__mem_end:
            dest = self.__overflow
            if loc not in dest.keys():  # haven't accessed this location yet
                dest[loc] = 0
        else:
            dest = self.__mem
        return dest[loc]

    def __current_instruction(self):
        return self.__val_at_location(self.__ip)

    def __val_at_offset(self, offset):
        return self.__val_at_location(self.__ip + offset)

    def __val_at_offset_by_mode(self, mode, offset):
        param = self.__val_at_location(self.__ip + offset)
        if mode == Mode.IMMEDIATE:
            return param
        elif mode == Mode.POSITION:
            return self.__val_at_location(param)
        elif mode == Mode.RELATIVE:
            return self.__val_at_location(self.__rel + param)
        else:
            raise InvalidAccessModeException

    def __address_at_offset_by_mode(self, mode, offset):
        val = self.__val_at_location(self.__ip + offset)
        if mode == Mode.RELATIVE:
            return self.__rel + val
        return val

    def __execute_instruction(self):
        nxt_dist = 4
        opcode = self.__current_instruction()
        inst = Instruction(opcode)
        if inst.operator == OPCodes.ADD:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__address_at_offset_by_mode(inst.write_mode, 3)
            self.__write_to_location(dest, lhs+rhs)
        elif inst.operator == OPCodes.MULT:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__address_at_offset_by_mode(inst.write_mode, 3)
            self.__write_to_location(dest, lhs*rhs)
        elif inst.operator == OPCodes.LT:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__address_at_offset_by_mode(inst.write_mode, 3)
            self.__write_to_location(dest, 1 if lhs < rhs else 0)
        elif inst.operator == OPCodes.EQ:
            lhs = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            rhs = self.__val_at_offset_by_mode(inst.rhs_mode, 2)
            dest = self.__address_at_offset_by_mode(inst.write_mode, 3)
            self.__write_to_location(dest, 1 if lhs == rhs else 0)
        elif inst.operator == OPCodes.JMPT:
            chk = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            nxt_dist = 3 if chk == 0 else (self.__val_at_offset_by_mode(inst.rhs_mode, 2) - self.__ip)
        elif inst.operator == OPCodes.JMPF:
            chk = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            nxt_dist = 3 if chk != 0 else (self.__val_at_offset_by_mode(inst.rhs_mode, 2) - self.__ip)
        elif inst.operator == OPCodes.IN:
            if self.__input_ready:
                read = self.__input_value
                self.__input_ready = False
                dest = self.__address_at_offset_by_mode(inst.lhs_mode, 1)
                self.__write_to_location(dest, read)
                self.__status = StatusCode.CONTINUE
                nxt_dist = 2
            else:
                self.__status = StatusCode.NEEDS_INPUT
                nxt_dist = 0  # if we're waiting for input we'll need to run this again
        elif inst.operator == OPCodes.OUT:
            if self.__output_consumed:
                self.__output_consumed = False
                self.__status = StatusCode.CONTINUE
                nxt_dist = 2
            else:
                self.__status = StatusCode.HAS_OUTPUT
                src = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
                self.__output_value = src
                nxt_dist = 0  # as with input, we must wait here until output has been consumed
        elif inst.operator == OPCodes.ADJ_REL:
            val = self.__val_at_offset_by_mode(inst.lhs_mode, 1)
            self.__increment_rel(val)
            nxt_dist = 2
        elif inst.operator == OPCodes.HALT:
            self.__status = StatusCode.HALT
            nxt_dist = 0
        self.__increment_ip(nxt_dist)

    def input(self, val):
        self.__input_value = val
        self.__input_ready = True

    def get_output(self):
        if not self.__status == StatusCode.HAS_OUTPUT:
            assert False, f"{self.name} had no output"  # sanity check, should never happen
        self.__output_consumed = True
        return self.__output_value

    def execute(self):
        while self.__status != StatusCode.HALT \
                and (self.__status == StatusCode.CONTINUE or self.__output_consumed or self.__input_ready):
            self.__execute_instruction()
        return self.__status
