from computer import Computer
from status_code import StatusCode

with open("9.in", "r") as file:
    prog = [int(x) for x in file.readlines()[0].split(",")]

comp = Computer("A", prog)

finished = False
while not finished:
    status = comp.execute()
    if status == StatusCode.HALT:
        finished = True
    elif status == StatusCode.NEEDS_INPUT:
        val = int(input("Computer needs an int: "))
        comp.input(val)
    elif status == StatusCode.HAS_OUTPUT:
        print(f"Computer brings glad tidings of {comp.get_output()}!")
