from enum import Enum, auto


class StatusCode(Enum):
    CONTINUE = auto()
    NEEDS_INPUT = auto()
    HAS_OUTPUT = auto()
    HALT = auto()
