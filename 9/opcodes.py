from enum import IntEnum


class OPCodes(IntEnum):
    ADD = 1
    MULT = 2
    IN = 3
    OUT = 4
    JMPT = 5
    JMPF = 6
    LT = 7
    EQ = 8
    ADJ_REL = 9
    HALT = 99

    @staticmethod
    def is_valid(op):
        return op in [OPCodes.HALT, OPCodes.ADD, OPCodes.MULT, OPCodes.IN,
                      OPCodes.OUT, OPCodes.JMPT, OPCodes.JMPF, OPCodes.LT,
                      OPCodes.EQ, OPCodes.ADJ_REL]
