def count_zeros(x):
    return x.count("0")


def calculate_checksum(img):
    img_to_sort = img.copy()
    img_to_sort.sort(key=count_zeros)
    return img_to_sort[0].count("1")*img_to_sort[0].count("2")


height = 6
width = 25
with open("8.in", "r") as file:
    recv = file.readlines()[0]
layer_size = height*width
layer_count = int(len(recv) / layer_size)
layers = []
for r in range(0, layer_count*layer_size, layer_size):
    layer = recv[r:r+layer_size]
    layers.append(layer)

print(f"Checksum is: {calculate_checksum(layers)}")

final_img = ""
for i in range(len(layers[0])):
    layer = 0
    pix = layers[layer][i]
    while pix == "2":
        layer += 1
        pix = layers[layer][i]
    if pix == "0":
        char = " "
    else:
        char = "█"
    final_img += char

print("Final image:")

for i in range(height):
    for j in range(width):
        pix = (i*width)+j
        print(final_img[pix], end='')
    print()
