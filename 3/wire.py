class Wire:
    __x = 0
    __y = 1

    def __init__(self, path):
        self.__path = path
        self.positions = set()
        self.__stepCounts = dict()
        self.__pos = (0, 0)
        self.__steps = 0

    def enumerate_path(self):
        while self.__move_step():
            pass
        return self

    def get_signal_distance(self, point):
        return self.__stepCounts[point]

    def get_crossover_points(self, other):
        return self.positions.intersection(other.positions)

    def __move_step(self):
        if self.__steps < len(self.__path):
            direction = self.__path[self.__steps]
            self.__do_step(direction)
            self.__steps += 1
            # we only want to store the MINIMAL distance to get to this point
            if self.__pos not in self.__stepCounts:
                self.__stepCounts[self.__pos] = self.__steps
            return True
        else:
            # If we get to the end of our path, just stop moving
            return False

    def __get_x(self):
        return self.__pos[self.__x]

    def __get_y(self):
        return self.__pos[self.__y]

    def __do_step(self, direction):
        if direction == 'R':
            self.__pos = (self.__get_x() + 1, self.__get_y())
        if direction == 'L':
            self.__pos = (self.__get_x() - 1, self.__get_y())
        if direction == 'U':
            self.__pos = (self.__get_x(), self.__get_y() + 1)
        if direction == 'D':
            self.__pos = (self.__get_x(), self.__get_y() - 1)
        self.positions.add(self.__pos)