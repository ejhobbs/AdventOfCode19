from wire import Wire


def distance(pos):
    return abs(pos[0]) + abs(pos[1])


def signal_distance(a, b, pos):
    return a.get_signal_distance(pos) + b.get_signal_distance(pos)


def repeat_instruction(i):
    direction = i[0]
    count = int(i[1:])
    repeated = []
    for i in range(count):
        repeated.append(direction)
    return repeated


def normalise_input(i):
    ops = i.split(',')
    instructions = []
    for op in ops:
        instructions.extend(repeat_instruction(op))
    return instructions


with open("3.in", "r") as file:
    wires = file.readlines()
    one = Wire(normalise_input(wires[0])).enumerate_path()
    two = Wire(normalise_input(wires[1])).enumerate_path()

collisions = one.get_crossover_points(two)
shortest_taxi_collision = min(collisions, key=distance)
shortest_signal_collision = min(collisions, key=lambda x: signal_distance(one, two, x))
print(f"Manhattan Distance: {distance(shortest_taxi_collision)}")
print(f"Signal Distance: {signal_distance(one, two, shortest_signal_collision)}")
