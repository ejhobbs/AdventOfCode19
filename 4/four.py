with open("4.in", "r") as f:
    search_range = f.readlines()[0].split("-")
    start = int(search_range[0])
    finish = int(search_range[1])


def is_ordered(s):
    for i in range(len(s) - 1):
        if s[i + 1] < s[i]:
            return False
    return True


def has_repeating_digit(s):
    for i in range(len(s) - 1):
        if s[i] == s[i + 1]:
            return True
    return False


def has_only_double_digits(s):
    # filter to unique digits
    for i in set(s):
        good = 2 * str(i)
        bad = good + str(i)
        if s.find(good) >= 0 > s.find(bad):
            return True
    return False


cur = start
part_one_count = 0
part_two_count = 0
while cur <= finish:
    cur_string = str(cur)
    cur_list = [int(x) for x in cur_string]
    if is_ordered(cur_list):
        if has_repeating_digit(cur_list):
            part_one_count += 1
        if has_only_double_digits(cur_string):
            part_two_count += 1
    cur += 1

print(f"Part One: {part_one_count}")
print(f"Part Two: {part_two_count}")
