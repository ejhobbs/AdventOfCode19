import computer

with open("2.in", "r") as file:
    prog = [int(x) for x in file.readlines()[0].split(',')]
    prog[1] = 12
    prog[2] = 2

print(computer.execute(prog))
