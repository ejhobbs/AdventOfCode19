class InvalidOpcodeException(object):
    pass


def get_operation(opcode):
    if opcode == 1:
        return lambda x, y: x + y
    elif opcode == 2:
        return lambda x, y: x * y
    else:
        raise InvalidOpcodeException


def apply_operation(prog, opcode, pt):
    operation = get_operation(opcode)
    prog[prog[pt + 3]] = operation(prog[prog[pt + 1]], prog[prog[pt + 2]])


def execute(prog):
    opp = 0
    op = prog[opp]
    while op != 99:
        apply_operation(prog, op, opp)
        opp += 4
        op = prog[opp]
    return prog[0]