import computer


def enumerate_values(program, noun_pos, verb_pos, expected):
    for i in range(0, 100):
        for j in range(0, 100):
            this_prog = program.copy()
            this_prog[noun_pos] = i
            this_prog[verb_pos] = j
            if computer.execute(this_prog) == expected:
                return i, j


with open("2.in", "r") as file:
    prog = [int(x) for x in file.readlines()[0].split(',')]


n, v = enumerate_values(prog, 1, 2, 19690720)
print((100 * n) + v)
